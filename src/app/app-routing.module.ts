import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GestionArticulosComponent } from './pages/gestion-articulos/gestion-articulos.component';
import { ModificadosComponent } from './pages/gestion-articulos/modificados/modificados.component';
import { PendientesComponent } from './pages/gestion-articulos/pendientes/pendientes.component';
import { BlogComponent } from './pages/blog/blog/blog.component';
import {  BlogabiertoComponent } from './pages/blog/blogabierto/blogabierto.component';
import { CrearPostComponent } from './pages/blog/crear-post/crear-post.component';
import { GestionUsuarioComponent } from './pages/gestion-usuario/gestion-usuario.component';
import { LoginComponent } from './pages/login/login.component';
import { PerfilAdminComponent } from './pages/perfil-admin/perfil-admin.component';
import { RevisarSolicitudesComponent } from './pages/revisar-solicitudes/revisar-solicitudes.component';
import { RecuperarContrasenaComponent } from './pages/RecuperarContrasena/recuperar-contrasena/recuperar-contrasena.component';
import { CodigorecComponent } from './pages/RecuperarContrasena/codigorec/codigorec.component';
import { IngreseNuevaContrasenaComponent } from './pages/RecuperarContrasena/ingrese-nueva-contrasena/ingrese-nueva-contrasena.component';
import { EstadisticasComponent } from './pages/estadisticas/estadisticas.component';
import { HomeComponent } from './pages/home/home.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { ArticuloModificadoComponent } from './pages/gestion-articulos/articulo-modificado/articulo-modificado.component';
import { EliminadosComponent } from './pages/gestion-articulos/eliminados/eliminados.component';
import { UsuarioComponent } from './pages/gestion-usuario/usuario/usuario.component';
import { CatalogoComponent } from './pages/catalogo/catalogo.component'
import { CatalogoAbiertoComponent } from './pages/catalogo/catalogo-abierto/catalogo-abierto.component'
import { GestionCatalogoComponent } from './pages/gestion-catalogo/gestion-catalogo.component';
import { SubirComponent } from './pages/gestion-catalogo/subir/subir.component';

const routes: Routes = [
  {
    path: 'gestion-usuarios', component:GestionUsuarioComponent
  },
  {
    path: 'gestion-usuarios/:id', component:UsuarioComponent
  },
  {
    path: 'gestion-articulos', component:GestionArticulosComponent
  },
  {
    path: 'gestion-articulos/pendientes', component:PendientesComponent
  },
  {
    path: 'gestion-articulos/eliminados', component:EliminadosComponent
  },
  {
    path: 'gestion-articulos/modificados', component:ModificadosComponent
  },
  {
    path: 'gestion-articulos/modificados/:id', component:ArticuloModificadoComponent
  },
  {
    path: 'gestion-catalogo', component:GestionCatalogoComponent
  },
  {
    path: 'gestion-catalogo/subir', component:SubirComponent
  },
  {
    path: 'login', component:LoginComponent
  },

  {
    path: 'home', component:HomeComponent
  },
  {
    path:"blog",component:BlogComponent
  },

  {
    path:"blog/:id",component: BlogabiertoComponent

  },
  {
    path: 'registro', component:RegistroComponent
  },

  {
    path:"blogabierto",component:  BlogabiertoComponent
  },

  {
    path:"crear-post",component:CrearPostComponent
  },

  {
    path:"perfil-admin/:id",component:PerfilAdminComponent
  },

  {
    path:"revisar-solicitudes",component:RevisarSolicitudesComponent
  },

  {
    path:"revisar-solicitudes/:id",component:RevisarSolicitudesComponent
  },

  {
    path:"recuperar-contrasena",component:RecuperarContrasenaComponent
  },

  {
    path:"codigorec",component: CodigorecComponent
  },


  {
    path:"ingrese-nueva-contrasena",component: IngreseNuevaContrasenaComponent
  },


  {
    path:"estadisticas",component: EstadisticasComponent
  },

  {
    path:"catalogo",component: CatalogoComponent
  },

  {
    path:"catalogo-abierto",component: CatalogoAbiertoComponent
  },

  {
    path: "**",
    pathMatch: "full",
    redirectTo: "home"
  },






];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
