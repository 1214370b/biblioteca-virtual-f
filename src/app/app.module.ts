import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GestionUsuarioComponent } from './pages/gestion-usuario/gestion-usuario.component';
import { PerfilAdminComponent } from './pages/perfil-admin/perfil-admin.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlogComponent } from './pages/blog/blog/blog.component';
import { CrearPostComponent } from './pages/blog/crear-post/crear-post.component';
import { BlogabiertoComponent } from './pages/blog/blogabierto/blogabierto.component';
import { GestionArticulosComponent } from './pages/gestion-articulos/gestion-articulos.component';
import { PendientesComponent } from './pages/gestion-articulos/pendientes/pendientes.component';
import { ModificadosComponent } from './pages/gestion-articulos/modificados/modificados.component';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { NavbarComponent } from './pages/navbar/navbar.component';
import { RecuperarContrasenaComponent } from './pages/RecuperarContrasena/recuperar-contrasena/recuperar-contrasena.component';
import { CodigorecComponent } from './pages/RecuperarContrasena/codigorec/codigorec.component';
import { IngreseNuevaContrasenaComponent } from './pages/RecuperarContrasena/ingrese-nueva-contrasena/ingrese-nueva-contrasena.component';
import { EstadisticasComponent } from './pages/estadisticas/estadisticas.component';
import { RevisarSolicitudesComponent } from './pages/revisar-solicitudes/revisar-solicitudes.component';
import { ChartComponent } from './chart/chart.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { ArticuloModificadoComponent } from './pages/gestion-articulos/articulo-modificado/articulo-modificado.component';
import { EliminadosComponent } from './pages/gestion-articulos/eliminados/eliminados.component';
import { UsuarioComponent } from './pages/gestion-usuario/usuario/usuario.component';
import { CatalogoComponent } from './pages/catalogo/catalogo.component';
import { CatalogoAbiertoComponent } from './pages/catalogo/catalogo-abierto/catalogo-abierto.component';
import { GestionCatalogoComponent } from './pages/gestion-catalogo/gestion-catalogo.component';
import { SubirComponent } from './pages/gestion-catalogo/subir/subir.component';


@NgModule({
  declarations: [

    AppComponent,
    GestionUsuarioComponent,
    GestionArticulosComponent,
    PendientesComponent,
    PerfilAdminComponent,
    BlogComponent,
    CrearPostComponent,
    BlogabiertoComponent,
    GestionUsuarioComponent,
    LoginComponent,
    HomeComponent,
    NavbarComponent,
    RecuperarContrasenaComponent,
    CodigorecComponent,
    IngreseNuevaContrasenaComponent,
    EstadisticasComponent,
    RevisarSolicitudesComponent,
    BlogabiertoComponent,
    ChartComponent,
    RegistroComponent,
    ModificadosComponent,
    ArticuloModificadoComponent,
    EliminadosComponent,
    UsuarioComponent,
    CatalogoComponent,
    CatalogoAbiertoComponent,
    GestionCatalogoComponent,
    SubirComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbPaginationModule,
    NgbAlertModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
