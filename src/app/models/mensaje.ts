export interface Mensaje {
    receptorId: number;
    emisorId: number;
    cuerpo: string;
}
