import { AutorArticulo } from "./autor-articulo";

export interface GArticulo {
    id: number;
    titulo: string;
    imagen: string;
    cuerpo: string;
    etiquetas: string;
    activo: boolean;
    fecha_de_registro: string;
    estatus: string;
    autor: AutorArticulo;
}
