import { GArticulo } from "./g-articulo";

export interface ArticuloModificado {
    id: number;
    titulo_nuevo: string;
    cuerpo_nuevo: string;
    imagen_nueva: string;
	revisada: boolean;
	fecha_de_registro:string;
	activo: boolean;
    articulo_original: GArticulo;
}
