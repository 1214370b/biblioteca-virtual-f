export interface Material {
    id: number;
    titulo: string;
    autor_del_doc: string;
    fecha: string;
    archivo: string;
    descripcion: string;
    etiquetas: string;
    activo: boolean;
    fecha_de_registro: string;
}
