export interface MaterialSubir {
    titulo: string;
    autor_del_doc: string;
    categoria: number;
    fecha: string;
    archivo: string;
    descripcion: string;
    etiquetas: string;
}


// ejemplo de body de postman
/*
{    
    "titulo": "material subido desde postman6",
    "autor_del_doc": "postman3",
    "categoria": "prueba",
    "fecha": "01/08/1994",
    "archivo": "doc/doc433y.doc",
    "descripcion": "adjfnfsdflaaldkndnf",
    "etiquetas": "prueba, postman"
}
*/