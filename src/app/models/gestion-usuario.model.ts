export interface GestionUsuario {
    id: number;
    Nombre: string;
    Apellido: string;
    correo: string;
    contrasena: string;
    fecha_de_nacimiento: string;
    nacionalidad: string;
    cv: string;
    activo: boolean;
    fecha_de_registro: string;
    rol: string;
}
