import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { RevisarSolicitudesComponent } from '../pages/revisar-solicitudes/revisar-solicitudes.component';

@Injectable({
  providedIn: 'root'
})
export class RevisarSolicitudesService {

  private MyBaseUrl: string = environment.BASE_API_URL;

  constructor( private readonly _http: HttpClient) { }

  public getsolicitudesid(id: number): Observable<Object>{
    return this._http.get<RevisarSolicitudesComponent[]>(this.MyBaseUrl + "solicitudes/"+ id)
    }
}
