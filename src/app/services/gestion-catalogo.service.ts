import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MaterialSubir } from '../models/material-subir.model';
import { Material } from '../models/material.model';

@Injectable({
  providedIn: 'root'
})
export class GestionCatalogoService {

  private MyBaseUrl: string = environment.BASE_API_URL;

  constructor(
    private readonly _http: HttpClient,
  ) { }


  public getMaterial(): Observable<Object>{
    return this._http.get<Material[]>( this.MyBaseUrl + "gestion-catalogo", {responseType: "json"} );
  }


  public subirMaterial(body: MaterialSubir):Observable<Object> {
    console.log("service body: " + body.titulo)
    return this._http.post<Material>( 
      this.MyBaseUrl + "gestion-catalogo", 
      body, 
      {responseType: "json"} 
    )
  }


}
