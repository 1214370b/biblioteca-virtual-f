import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import { GestionUsuario } from '../models/gestion-usuario.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GestionUsuarioService {
  private MyBaseUrl: string = environment.BASE_API_URL;

  constructor(
    private readonly _http: HttpClient,
    ) { }

  public getUsuarios(): Observable<Object>{
    return this._http.get<GestionUsuario[]>(this.MyBaseUrl + "usuarios", {responseType: "json"})
  }

  public getUsuario(id: number){
    return this._http.get<GestionUsuario>( this.MyBaseUrl + "usuarios/" + id )
  }
}
