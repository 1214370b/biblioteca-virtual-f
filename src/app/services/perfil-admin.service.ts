import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Perfiladmin } from '../models/perfil-admin.models';

@Injectable({
  providedIn: 'root'
})


export class PerfilAdminService {

  private MyBaseUrl: string = environment.BASE_API_URL;

 constructor( private readonly _http: HttpClient) { }

 public getUsuariosid(id: number): Observable<Object>{
  return this._http.get<Perfiladmin>(this.MyBaseUrl + "usuarios/" + id)
  }
}
