import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})


export class BlogService {

  private MyBaseUrl: string = environment.BASE_API_URL;

  constructor( private readonly _http: HttpClient) { }

  public getarticulos(): Observable <Object>{
    return this._http.get<BlogService[]>(this.MyBaseUrl + "blog", {responseType: "json"})



  }

  public getcategoria(): Observable <Object>{
    return this._http.get<BlogService[]>(this.MyBaseUrl + "categorias", {responseType: "json"})




  }
}
