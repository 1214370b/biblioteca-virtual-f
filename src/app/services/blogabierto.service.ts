import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BlogabiertoService {

  private MyBaseUrl: string = environment.BASE_API_URL;

  constructor(private readonly _http: HttpClient) { }

  public getarticulosid(id: number){
    return this._http.get<BlogabiertoService[]>( this.MyBaseUrl + "blog/" + id )
  }


}
