import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ArticuloModificado } from '../models/articulo-modificado.model';
import { GArticulo } from '../models/g-articulo';
import { Mensaje } from '../models/mensaje';

@Injectable({
  providedIn: 'root'
})
export class GestionArticulosService {
  
  private MyBaseUrl: string = environment.BASE_API_URL;
  
  constructor(
    private readonly _http: HttpClient,
  ) { }

    public getArticulos(): Observable<Object>{
      return this._http.get<GArticulo[]>( this.MyBaseUrl + "gestion-articulos", {responseType: "json"} );
    }

    public getArticuloPendientes(): Observable<Object>{
      return this._http.get<GArticulo[]>( this.MyBaseUrl + "gestion-articulos/pendientes", {responseType: "json"} );
    }

    public aceptarArticulo( id: number ): Observable<Object> {
      return this._http.put<GArticulo>(
        this.MyBaseUrl + "gestion-articulos/aceptar/" + id,
        null,
        {responseType: "json"}
      );
    }

    public rechazarArticulo( id: number, body: Mensaje ): Observable<Object> {
      return this._http.put<GArticulo>(
        this.MyBaseUrl + "gestion-articulos/rechazar/" + id,
        body,
        {responseType: "json"}
      );
    }

    public getArticulosEliminados(): Observable<Object>{
      return this._http.get<GArticulo[]>( this.MyBaseUrl + "gestion-articulos/eliminados", {responseType: "json"} );
    }

    public getArticulosModificados(): Observable<Object>{
      return this._http.get<ArticuloModificado[]>( this.MyBaseUrl + "gestion-articulos/modificados", {responseType: "json"} );
    }

    public getArticuloModificado( id: number ): Observable<Object>{
      return this._http.get<ArticuloModificado>( this.MyBaseUrl + "gestion-articulos/modificados/" + id, {responseType: "json"} );
    }

    public aceptarCambios( id: number ): Observable<Object> {
      return this._http.put<ArticuloModificado>( 
        this.MyBaseUrl + "gestion-articulos/modificados/aceptar/" + id, 
        null, 
        {responseType: "json"} 
      );
    }

    public rechazarCambios( id: number, body: Mensaje ): Observable<Object> {
      return this._http.put<ArticuloModificado>( 
        this.MyBaseUrl + "gestion-articulos/modificados/rechazar/" + id, 
        body, 
        {responseType: "json"} 
      );
    }
}
