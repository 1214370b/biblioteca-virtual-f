import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Perfiladmin } from 'src/app/models/perfil-admin.models';
import { PerfilAdminService } from 'src/app/services/perfil-admin.service';

@Component({
  selector: 'app-perfil-admin',
  templateUrl: './perfil-admin.component.html',
  styleUrls: ['./perfil-admin.component.css']
})
export class PerfilAdminComponent implements OnInit {
  public Perfiladmin:Perfiladmin ={

  id: 0,
  Nombre: "",
  Apellido:"",
  correo: "",
  contrasena: "",
  fecha_de_nacimiento: "",
  nacionalidad: "",
  cv: "",
  fecha_de_registro:"",
  rol: "",
  activo: true
  }

  constructor(
    public perfiladminService: PerfilAdminService,
    private _activatedRoute:ActivatedRoute
  ) { }

  ngOnInit(): void {
    this._activatedRoute.params.subscribe(
      (           params: { [x: string]: number; }) => {

        const id: number = params["id"];

        this.perfiladminService.getUsuariosid(id).subscribe(
           Perfiladmin => {
            this.Perfiladmin = (<Perfiladmin> Perfiladmin);
          }

        )
      }
    )

  }

}
