import { Component, OnInit } from '@angular/core';
import { GestionUsuario } from 'src/app/models/gestion-usuario.model';
import { GestionUsuarioService } from 'src/app/services/gestion-usuario.service';

@Component({
  selector: 'app-gestion-usuario',
  templateUrl: './gestion-usuario.component.html',
  styleUrls: ['./gestion-usuario.component.css']
})
export class GestionUsuarioComponent implements OnInit {
  
  public usuarios?: GestionUsuario[];

  constructor(
    public gestionUsuarioService: GestionUsuarioService,
    ) { }

  ngOnInit(): void {
    this.gestionUsuarioService.getUsuarios().subscribe(
      usuarios => {
        this.usuarios = (<GestionUsuario[]> usuarios);
      }
    )
  }


  

}
