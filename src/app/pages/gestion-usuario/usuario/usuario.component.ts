import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GestionUsuario } from 'src/app/models/gestion-usuario.model';
import { GestionUsuarioService } from 'src/app/services/gestion-usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  public usuario: GestionUsuario = {
    id: 0,
    Nombre: "",
    Apellido: "",
    correo: "",
    contrasena: "",
    fecha_de_nacimiento: "",
    nacionalidad: "",
    cv: "",
    activo: true,
    fecha_de_registro: "",
    rol: ""
  }

  constructor(
    public gestionUsuarioService: GestionUsuarioService,
    private _activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {

    this._activatedRoute.params.subscribe( 
      params => {

        const id: number = params["id"];

        this.gestionUsuarioService.getUsuario(id).subscribe(
          usuario => {
            this.usuario = (<GestionUsuario> usuario);
          }
          
        )
      }
    )

  }

}
