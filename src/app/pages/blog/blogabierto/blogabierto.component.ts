import { Component, OnInit } from '@angular/core';
import { blogabierto } from 'src/app/models/blogabierto.model';
import { BlogabiertoService } from 'src/app/services/blogabierto.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-blogabierto',
  templateUrl: './blogabierto.component.html',
  styleUrls: ['./blogabierto.component.css']
})
export class BlogabiertoComponent implements OnInit {


  public blog:blogabierto = {
    titulo: "",
    categoria: "",
    cuerpo: "",
    _id: '',
    imagen: '',
    etiquetas: [],
    fecha:''
  }




  constructor(public blogabiertoservice:BlogabiertoService,
    private _activatedRoute:ActivatedRoute) { }

  ngOnInit(): void {

    this._activatedRoute.params.subscribe(
      (           params: { [x: string]: number; }) => {

        const id: number = params["id"];

        this.blogabiertoservice.getarticulosid(id).subscribe(
          blog => {
            this.blog = (<blogabierto> <unknown> blog);
            console.log(blog)
          }

        )
      }
    )

  }

}
