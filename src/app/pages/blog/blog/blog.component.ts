import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { Component, OnInit } from '@angular/core';
import { blog } from 'src/app/models/blog.model';
import { categoria } from 'src/app/models/categoria.model';
import { BlogService } from 'src/app/services/blog.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  public blogs?: blog[];
  public categorias?: categoria[];
  private _activatedRoute: any;



  constructor(
    public blogservice:BlogService

  ) { }

  ngOnInit(): void {

    this.blogservice.getarticulos().subscribe(
      blogs => {
        this.blogs = (<blog[]> blogs);

      }

    )

    this.blogservice.getcategoria().subscribe(
      categoria => {
        this.categorias=(<categoria[]>categoria)
      }
    )


  }

}
