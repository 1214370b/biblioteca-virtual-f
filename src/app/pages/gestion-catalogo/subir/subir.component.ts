import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MaterialSubir } from 'src/app/models/material-subir.model';
import { Material } from 'src/app/models/material.model';
import { GestionCatalogoService } from 'src/app/services/gestion-catalogo.service';

@Component({
  selector: 'app-subir',
  templateUrl: './subir.component.html',
  styleUrls: ['./subir.component.css']
})
export class SubirComponent implements OnInit {

  public materialNuevo: MaterialSubir ={
    titulo: "",
    autor_del_doc: "",
    categoria: 1,
    fecha: "",
    archivo: "",
    descripcion: "",
    etiquetas: ""
  }

  checkoutForm = this.formBuilder.group({
    titulo: '',
    autor_del_doc: '',
    categoria: '',
    fecha: '',
    archivo: '',
    descripcion: '',
    etiquetas: '',
  });

  fileName = '';
  public formData = new FormData();

  constructor(
    public gestionCatalogoService: GestionCatalogoService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
  }



  async onSubmit(event: any): Promise<void> {
    const file:File = event.target.files[0];

    if (file) {

      this.fileName = file.name;

     // this.formData.append("thumbnail", file);
      //console.log(this.formData)

      this.materialNuevo = this.checkoutForm.value;
      await file.text().then(text => {
        //console.log(text);
        this.materialNuevo.archivo = text;
        this.materialNuevo
        console.log(this.materialNuevo);
        this.gestionCatalogoService.subirMaterial(this.materialNuevo).subscribe(
          respuesta => { console.log(respuesta) }
        )
      });
      
      //this.checkoutForm.reset();
    }

    
  }


  

}
