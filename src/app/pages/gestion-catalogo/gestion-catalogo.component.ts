import { Component, OnInit } from '@angular/core';
import { Material } from 'src/app/models/material.model';
import { GestionCatalogoService } from 'src/app/services/gestion-catalogo.service';

@Component({
  selector: 'app-gestion-catalogo',
  templateUrl: './gestion-catalogo.component.html',
  styleUrls: ['./gestion-catalogo.component.css']
})
export class GestionCatalogoComponent implements OnInit {

  public materiales?: Material[];

  constructor(
    public gestionCatalogoService: GestionCatalogoService,
  ) { }

  ngOnInit(): void {

    this.gestionCatalogoService.getMaterial().subscribe(
      materiales => {
        this.materiales = (<Material[]> materiales);
      }
    )

  }

}
