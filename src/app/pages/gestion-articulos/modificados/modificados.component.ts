import { Component, OnInit } from '@angular/core';
import { ArticuloModificado } from 'src/app/models/articulo-modificado.model';
import { GArticulo } from 'src/app/models/g-articulo';
import { GestionArticulosService } from 'src/app/services/gestion-articulos.service';

@Component({
  selector: 'app-modificados',
  templateUrl: './modificados.component.html',
  styleUrls: ['./modificados.component.css']
})
export class ModificadosComponent implements OnInit {
  public articulos?: ArticuloModificado[];

  constructor(
    public gestionArticulosService: GestionArticulosService,
  ) { }

  ngOnInit(): void {
    this.gestionArticulosService.getArticulosModificados().subscribe(
      articulos => {
        this.articulos = (<ArticuloModificado[]> articulos);
      }
    )
    
  }

}
