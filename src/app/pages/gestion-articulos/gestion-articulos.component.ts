import { Component, OnInit } from '@angular/core';
import { GArticulo } from 'src/app/models/g-articulo';
import { GestionArticulosService } from 'src/app/services/gestion-articulos.service';

@Component({
  selector: 'app-gestion-articulos',
  templateUrl: './gestion-articulos.component.html',
  styleUrls: ['./gestion-articulos.component.css']
})
export class GestionArticulosComponent implements OnInit {
  public articulos?: GArticulo[];

  constructor(
    public gestionArticulosService: GestionArticulosService,
  ) { }

  ngOnInit(): void {
    this.gestionArticulosService.getArticulos().subscribe(
      articulos => {
        this.articulos = (<GArticulo[]> articulos);
      }
    )
  }

}
