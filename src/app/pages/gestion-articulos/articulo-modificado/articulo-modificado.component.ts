import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticuloModificado } from 'src/app/models/articulo-modificado.model';
import { Mensaje } from 'src/app/models/mensaje';
import { GestionArticulosService } from 'src/app/services/gestion-articulos.service';

@Component({
  selector: 'app-articulo-modificado',
  templateUrl: './articulo-modificado.component.html',
  styleUrls: ['./articulo-modificado.component.css']
})
export class ArticuloModificadoComponent implements OnInit {
  public articuloM: ArticuloModificado = {
    id: 0,
    titulo_nuevo: "",
    cuerpo_nuevo: "",
    imagen_nueva: "",
    revisada: false,
    fecha_de_registro:"",
    activo: true,
    articulo_original: {
      id: 0,
      titulo: "",
      imagen: "",
      cuerpo: "",
      etiquetas: "",
      activo: true,
      fecha_de_registro: "",
      estatus: "",
      autor: {
        id: 0,
        Nombre: ""
      }
    }
  }

  public mensaje: Mensaje = {
    receptorId: 0,
    emisorId: 0,
    cuerpo: ""
  }

  constructor(
    public gestionArticulosService: GestionArticulosService,
    private _activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {

    this._activatedRoute.params.subscribe(params => {

      const id: number = params["id"];

      this.gestionArticulosService
        .getArticuloModificado(id)
        .subscribe(
          articuloM => {
            this.articuloM = (<ArticuloModificado> articuloM);
          }
        )

    });

  }

  aceptarCambios( id: number ): void {
    console.log("id: " + id);
    this.gestionArticulosService.aceptarCambios( id ).subscribe(
      respuesta => {
        console.log(respuesta);
      }
    )
  }

  rechazarCambios( id: number, mensajeC: string, receptor: number, emisor: number ): void {
    console.log("id: " + id, "autorID: " + receptor );
    this.mensaje.cuerpo = mensajeC;
    this.mensaje.emisorId = emisor;
    this.mensaje.receptorId = receptor;
    
    this.gestionArticulosService.rechazarCambios( id, this.mensaje ).subscribe(
      respuesta => {
        console.log(respuesta);
      }
    )
  }

}
