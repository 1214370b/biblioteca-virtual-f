import { Component, OnInit } from '@angular/core';


import { GArticulo } from 'src/app/models/g-articulo';
import { Mensaje } from 'src/app/models/mensaje';
import { GestionArticulosService } from 'src/app/services/gestion-articulos.service';


@Component({
  selector: 'app-pendientes',
  templateUrl: './pendientes.component.html',
  styleUrls: ['./pendientes.component.css']
})


export class PendientesComponent implements OnInit {
  public articulos?: GArticulo[];

  public mensaje: Mensaje = {
    receptorId: 0,
    emisorId: 0,
    cuerpo: ""
  }

  constructor(
    public gestionArticulosService: GestionArticulosService,
  ) { }

  ngOnInit(): void {
    
    this.gestionArticulosService.getArticuloPendientes().subscribe(
      articulos => {
        this.articulos = ( <GArticulo[]> articulos );
      }
    )
    
  }


  aceptarArticulo(id: number): void {
    this.gestionArticulosService.aceptarArticulo(id).subscribe(
      respuesta => {
        console.log(respuesta);
        window.location.reload();
      }
    )
  }


  rechazarArticulo( id: number, mensajeC: string, receptor: number, emisor: number ): void {
    console.log("id: " + id, "autorID: " + receptor );
    this.mensaje.cuerpo = mensajeC;
    this.mensaje.emisorId = emisor;
    this.mensaje.receptorId = receptor;
    
    this.gestionArticulosService.rechazarArticulo( id, this.mensaje ).subscribe(
      respuesta => {
        console.log(respuesta);
        window.location.reload();
      }
    )
  }


}
