import { Component, OnInit } from '@angular/core';
import { GArticulo } from 'src/app/models/g-articulo';
import { GestionArticulosService } from 'src/app/services/gestion-articulos.service';

@Component({
  selector: 'app-eliminados',
  templateUrl: './eliminados.component.html',
  styleUrls: ['./eliminados.component.css']
})
export class EliminadosComponent implements OnInit {
  public articulos?: GArticulo[];

  constructor(
    public gestionArticulosService: GestionArticulosService,
  ) { }

  ngOnInit(): void {
    this.gestionArticulosService.getArticulosEliminados().subscribe(
      articulos => {
        this.articulos = (<GArticulo[]> articulos);
      }
    )
  }

}
