import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { blogabierto } from 'src/app/models/blogabierto.model';
import { solicitudes } from 'src/app/models/solicitudes.model';
import { RevisarSolicitudesService } from 'src/app/services/revisar-solicitudes.service';


@Component({
  selector: 'app-revisar-solicitudes',
  templateUrl: './revisar-solicitudes.component.html',
  styleUrls: ['./revisar-solicitudes.component.css']
})

export class RevisarSolicitudesComponent implements OnInit {

 public solicitudes:solicitudes={

  id:'',
  num_de_peticiones:"",
  Apellido: "",
  revisada: "",
  fecha_de_registro:"",
  activo:true,
  rol: ""

 }


 constructor(
  public revisarsolicitudesservice:RevisarSolicitudesService,
  private _activatedRoute: ActivatedRoute,
) { }




  ngOnInit(): void {

    this._activatedRoute.params.subscribe(
      (           params: { [x: string]: number; }) => {

        const id: number = params["id"];

        this.revisarsolicitudesservice.getsolicitudesid(id).subscribe(
          solicitudes => {
            this.solicitudes = (<solicitudes> <unknown> solicitudes);
            console.log(solicitudes)
          }

        )
      }
    )
  }

}
